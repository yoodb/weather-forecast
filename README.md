# forecast-weather

## 面试推荐

关注“Java精选”公众号，回复“Java面试”关键词，在线随时随地刷面试题，内涵3000+面试题，6000+道选择题。

**Java精选** 公众号专注于程序员推送一些Java开发知识，包括基础知识、各大流行框架（Mybatis、Spring、Spring Boot、Spring Cloud等）、大数据技术（Storm、Hadoop、MapReduce、Spark等）、数据库（Mysql、Oracle、NoSQL等）、算法与数据结构、面试专题、面试技巧经验、职业规划以及优质开源项目等。其中一部分由小编总结整理，另一部分来源于网络上优质资源，希望对大家的学习和工作有所帮助。

## 平台简介

专属县级或非直辖市区域查询天气预报；支持根据国内省、市、县获取明日天气预报情况，支持地区追加查看天气情况，保留历史查询地区记录；

距离比较近的市直辖区已剔除（比如郑州包含二七区等已删除），方便用户手机端查询；支持历史记录重置等等；

支持手机端和PC端；

## 技术栈

Vue 2.6版本，前端天气预报项目，不依赖后端服务，前端缓存县级地区数据历史记录。

## 安装启动

### 安装依赖
```
npm install
```

### 启动服务
```
npm run serve
```

### 构建环境
```
npm run build
```

### 预览图

<table>
    <tr>
        <td><img src="https://gitee.com/yoodb/weather-forecast/raw/master/src/imgs/1.png"/></td>
    </tr>
</table>